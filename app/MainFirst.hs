module MainFirst where

import MapWithProofsFirst
import NamedFirst
import qualified Data.Map.Lazy as M
import Prelude hiding (lookup)

data AppError =
  InvalidKey Integer | SomeOtherProblems
  deriving (Show)

-- | Validate key and map and potentially return an appropriate AppError.
validateMapKey ::
  Named keyName Integer ->
  Named mapName (Map Integer String) ->
  Either AppError (Member keyName mapName)
validateMapKey key map =
  case member key map of
    Just proof -> Right proof
    Nothing -> Left $ InvalidKey (forgetName key)

processMapKey ::
  Integer ->
  Map Integer String ->
  String
processMapKey key map =
  let
    zeroNamed :: Named zeroName Integer
    zeroNamed = name 0
    keyNamed :: Named keyName Integer
    keyNamed = name key
    mapNamed :: Named mapName (Map Integer String)
    mapNamed = name map
  in case validateMapKey zeroNamed mapNamed of
    Left appError -> "Error: " <> show appError
    Right proof -> lookup keyNamed mapNamed proof

main :: IO ()
main = pure ()
