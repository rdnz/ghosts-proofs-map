{-# language
  TypeOperators,
  ScopedTypeVariables
#-}

module Main where

import MapWithProofs
import Named
import qualified Data.Map.Lazy as M
import Data.Void (absurd)
import Prelude hiding (lookup)

data AppError =
  InvalidKey Integer | SomeOtherProblems
  deriving (Show)

-- | Validate key and map and potentially return an appropriate AppError.
validateMapKey ::
  Named keyName Integer ->
  Named mapName (Map Integer String) ->
  Either AppError (Member keyName mapName)
validateMapKey key map =
  case member key map of
    Just proof -> Right proof
    Nothing -> Left $ InvalidKey (forgetName key)

processMapKey ::
  Integer ->
  Map Integer String ->
  String
processMapKey key map =
  name key $ \(keyNamed :: Named keyName Integer) ->
  name map $ \(mapNamed :: Named mapName (Map Integer String)) ->
  case validateMapKey keyNamed mapNamed of
    Left appError -> "Error: " <> show appError
    Right proof -> lookup keyNamed mapNamed proof

myKeySubset ::
  map1 `KeySubset` map2 ->
  Member key map1 ->
  Member key map2
myKeySubset proof1 proof2 =
  case keySubset of
    Right proof -> proof
    Left implication -> absurd $ implication (proof1, proof2)

insertExample :: String
insertExample =
  name 0 $ \key0 ->
  name 1 $ \key1 ->
  name (M.singleton 0 "a") $ \mapOld ->
  insert key1 "b" mapOld $ \(mapNew, _proofKey, proofSubset) ->
  case member key0 mapOld of
    Nothing -> error "impossible because `0` is member of `M.singleton 0 a`"
    Just proof -> lookup key0 mapNew (myKeySubset proofSubset proof)

main :: IO ()
main = pure ()

