module MainExpand where

import qualified Data.Map.Lazy as M
import Prelude hiding (lookup)

data AppError =
  InvalidKey Integer | SomeOtherProblems
  deriving (Show)

-- | Validate key and map and potentially return an appropriate AppError.
validateMapKey ::
  Integer ->
  M.Map Integer String ->
  Maybe AppError
validateMapKey key map =
  case M.member key map of
    False -> Nothing
    True -> Just $ InvalidKey key

processMapKey ::
  Integer ->
  M.Map Integer String ->
  String
processMapKey key map =
  case validateMapKey key map of
    Just appError -> "Error: " <> show appError
    Nothing ->
      case M.lookup key map of
        Nothing -> error "impossible because we just applied validateMapKey"
        Just result -> result

main :: IO ()
main = pure ()
