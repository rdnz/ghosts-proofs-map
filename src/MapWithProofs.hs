{-# language
  TypeOperators,
  RankNTypes,
  RoleAnnotations
#-}

module MapWithProofs
  (
    M.Map,
    Member (),
    member,
    lookup,
    KeySubset (),
    keySubset,
    insert,
  )
where

import Named
import qualified Data.Map.Lazy as M
import Prelude hiding (lookup)
import Data.Void (Void)

data Member key map = Member
type role Member representational representational

member ::
  (Ord key) =>
  Named keyName key ->
  Named mapName (M.Map key _value) ->
  Maybe (Member keyName mapName)
member key map =
  if M.member (forgetName key) (forgetName map)
  then Just Member
  else Nothing

lookup ::
  (Ord key) =>
  Named keyName key ->
  Named mapName (M.Map key value) ->
  Member keyName mapName ->
  value
lookup key map _proof =
  case M.lookup (forgetName key) (forgetName map) of
    Just result -> result
    Nothing -> error "impossible because we have proof"

insert ::
  (Ord key) =>
  Named keyName key ->
  value ->
  Named mapName (M.Map key value) ->
  (forall resultName. InsertResult keyName key value mapName resultName -> r) ->
  r
insert key value map continuation =
  name (M.insert (forgetName key) value (forgetName map)) $ \result ->
  continuation (result, Member, KeySubset)

type InsertResult keyName key value mapName resultName =
  (
    Named resultName (M.Map key value),
    Member keyName resultName,
    mapName `KeySubset` resultName
  )

data map1 `KeySubset` map2 = KeySubset

keySubset ::
  Either
    ((map1 `KeySubset` map2, Member key map1) -> Void)
    (Member key map2)
keySubset = Right Member
