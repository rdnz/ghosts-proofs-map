module MapWithProofsFirst
  (
    M.Map,
    Member (),
    member,
    lookup,
  )
where

import NamedFirst
import qualified Data.Map.Lazy as M
import Prelude hiding (lookup)

data Member key map = Member

member ::
  (Ord key) =>
  Named keyName key ->
  Named mapName (M.Map key _value) ->
  Maybe (Member keyName mapName)
member key map =
  if M.member (forgetName key) (forgetName map)
  then Just Member
  else Nothing

lookup ::
  (Ord key) =>
  Named keyName key ->
  Named mapName (M.Map key value) ->
  Member keyName mapName ->
  value
lookup key map _proof =
  case M.lookup (forgetName key) (forgetName map) of
    Just result -> result
    Nothing -> error "impossible because we have proof"
