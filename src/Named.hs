{-# language
  RankNTypes,
  RoleAnnotations
#-}

module Named (Named (), name, forgetName) where

newtype Named name a = Named a
type role Named representational representational

name ::
  a ->
  (forall name. Named name a -> result) ->
  result
name a continuation = continuation (Named a)

forgetName :: Named name a -> a
forgetName (Named a) = a
