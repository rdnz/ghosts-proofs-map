module NamedFirst (Named (), name, forgetName) where

newtype Named name a = Named a

name ::
  a -> Named name a
name a = Named a

forgetName :: Named name a -> a
forgetName (Named a) = a
